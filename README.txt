
 $$$$$$\                               $$\    $$\ $$\           
$$  __$$\                              $$ |   $$ |\__|          
$$ /  \__| $$$$$$\  $$$$$$$\   $$$$$$\ $$ |   $$ |$$\ $$$$$$$$\ 
$$ |$$$$\ $$  __$$\ $$  __$$\ $$  __$$\\$$\  $$  |$$ |\____$$  |
$$ |\_$$ |$$$$$$$$ |$$ |  $$ |$$ /  $$ |\$$\$$  / $$ |  $$$$ _/ 
$$ |  $$ |$$   ____|$$ |  $$ |$$ |  $$ | \$$$  /  $$ | $$  _/   
\$$$$$$  |\$$$$$$$\ $$ |  $$ |\$$$$$$  |  \$  /   $$ |$$$$$$$$\ 
 \______/  \_______|\__|  \__| \______/    \_/    \__|\________|
                                                                
                                                                                                          
----------------- GENOVIZ ----------------------------
The GenoViz SDK provides re-usable components for genomics 
data visualization.

This source code is released under the Common Public License, v1.0, 
an OSI approved open source license.

See http://genoviz.sourceforge.net

GenoViz is hosted on SourceForge.net at 
http://sourceforge.net/projects/genoviz